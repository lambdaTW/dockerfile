docker run -p 0.0.0.0:16886:5050 \
-e "PGADMIN_DEFAULT_EMAIL=samwu@ponddy-edu.com" \
-e "PGADMIN_LISTEN_PORT=5050" \
-e "PGADMIN_LISTEN_ADDRESS=0.0.0.0" \
-e "PGADMIN_DEFAULT_PASSWORD=P@ssword" \
--name pgadmin \
--link pg:pg \
-d dpage/pgadmin4
